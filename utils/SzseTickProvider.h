#ifndef BB_CLIENTCORE_SZSETICKPROVIDER_H
#define BB_CLIENTCORE_SZSETICKPROVIDER_H

#include <bb/core/messages.h>
#include <bb/core/protobuf/ProtoBufMsg.h>
#include <bb/clientcore/TickProvider.h>
#include <bb/clientcore/MsgHandler.h>

namespace bb {

class SzseTickProvider : public TickProviderImpl
{
public:
    SzseTickProvider(const ClientContextPtr &ctx, const instrument_t &instr, source_t source, const std::string &desc);
    ~SzseTickProvider() {}

    virtual bool isLastTickOK() const { return m_bTickReceived; }

    uint64_t getNotionalTurnover() const { return m_notionalTurnover; }

private:
    void onTradeMsg( const ProtoBufMsgBase &msg );

    bool m_bTickReceived;

    uint64_t m_notionalTurnover;

    MsgHandlerPtr m_subTradeMsg;
    MsgHandlerPtr m_subIndexMsg;
};

BB_DECLARE_SHARED_PTR(SzseTickProvider);

}

#endif // BB_CLIENTCORE_SZSETICKPROVIDER_H
