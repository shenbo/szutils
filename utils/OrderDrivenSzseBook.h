#ifndef BB_CLIENTCORE_ORDERDRIVEN_SZSEBOOK_H
#define BB_CLIENTCORE_ORDERDRIVEN_SZSEBOOK_H

#include <map>
#include <set>

#include <bb/core/messages.h>
#include <bb/core/protobuf/ProtoBufMsg.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/Book.h>
#include <bb/clientcore/SourceBooks.h>

#include <boost/array.hpp>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR(ClientContext);
BB_FWD_DECLARE_SHARED_PTR(SourceMonitor);

class OrderDrivenSzseBook
    : public BookImpl
    , public ExtraMktInfoBook
    , private IEventDistListener
    , private SourceMonitorListener
{
public:
    friend class ShseBookTester;

    OrderDrivenSzseBook(const instrument_t& instr, ClientContextPtr spCC, source_t src,
             const std::string& strDesc, SourceMonitorPtr spSMon);
    virtual ~OrderDrivenSzseBook();

    virtual void flushBook();
    virtual PriceSize getNthSide(size_t depth, side_t side) const;
    virtual IBookLevelCIterPtr getBookLevelIter(side_t side) const;
    virtual size_t getNumLevels(side_t side) const;

    struct OrderInfo {
        OrderInfo(BookLevelPtr b, BookOrder* o)
            : bl(b)
            , order(o) {}
        BookLevelPtr bl;
        BookOrder* order;
    };
    typedef bb::shared_ptr<OrderInfo>   OrderInfoPtr;
    typedef std::list<BookLevelPtr>   BookLevelList;
    virtual double getMidPrice() const;

    virtual void onEvent(const Msg& msg);
    void onMarketTbt(const ProtoBufMsgBase& msg, acr::SzseMarketTbt* mkt);
    void updateSideLevel(BookLevelList& sideLevels, OrderInfoPtr info, uint64_t id, int64_t size, bool isCancelled);
    void onTickTbt(const ProtoBufMsgBase& msg, acr::SzseTickTbt* tick);

private:
    typedef std::map<uint64_t, OrderInfoPtr>   SeqNumMap;
    typedef std::bitset<32> SeenLevelsSet;
    typedef std::set<long> PriceSet;

    void onEvent(const ProtoBufMsgBase& msg);
    void onBadState();

    virtual void onSMonStatusChange(source_t src, smon_status_t status, smon_status_t old_status,
            const ptime_duration_t& avg_lag_tv, const ptime_duration_t& last_interval_tv);

    template<side_t side>
    void tryAddOrder2Level(const ProtoBufMsgBase& msg, const double price, const int32_t orderSize, const uint64_t orderID);

    ClientContextPtr    m_spCC;
    SourceMonitorPtr    m_spSMon;
    uint32_t            m_verbose;

    mutable bool        m_ok;
    mutable bool        m_ok_dirty;

    BookLevelList        m_bookLevels[2];    // BID = 0, ASK = 1
    SeqNumMap           m_seqNums;

    EventSubPtr         m_eventSub;
};
BB_DECLARE_SHARED_PTR(OrderDrivenSzseBook);

/// BookSpec corresponding to an Wind book
class OrderDrivenSzseBookSpec : public SourceBookSpec
{
public:
    BB_DECLARE_SCRIPTING();

    OrderDrivenSzseBookSpec() : SourceBookSpec() {}
    OrderDrivenSzseBookSpec(const bb::OrderDrivenSzseBookSpec&, const boost::optional<bb::InstrSubst>&);
    OrderDrivenSzseBookSpec(const instrument_t &instr, source_t src)
        : SourceBookSpec(instr, src)
    {}

    virtual IBookPtr build(BookBuilder *builder) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual OrderDrivenSzseBookSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
};
BB_DECLARE_SHARED_PTR(OrderDrivenSzseBookSpec);

}

#endif // BB_CLIENTCORE_ORDERDRIVEN_SZSEBOOK_H
