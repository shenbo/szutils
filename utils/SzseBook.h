#ifndef BB_CLIENTCORE_SZSEBOOK_H
#define BB_CLIENTCORE_SZSEBOOK_H

#include <map>
#include <set>

#include <bb/core/messages.h>
#include <bb/core/protobuf/ProtoBufMsg.h>
#include <bb/clientcore/ClockMonitor.h>
#include <bb/clientcore/Book.h>
#include <bb/clientcore/SourceBooks.h>

#include <boost/array.hpp>

namespace bb {

BB_FWD_DECLARE_SHARED_PTR(ClientContext);
BB_FWD_DECLARE_SHARED_PTR(SourceMonitor);

class SzseBook
    : public BookImpl
    , public ExtraMktInfoBook
    , private IEventDistListener
    , private SourceMonitorListener
{
public:
    friend class ShseBookTester;

    SzseBook(const instrument_t& instr, ClientContextPtr spCC, source_t src,
             const std::string& strDesc, SourceMonitorPtr spSMon);
    virtual ~SzseBook();

    virtual void flushBook();
    virtual PriceSize getNthSide(size_t depth, side_t side) const;
    virtual IBookLevelCIterPtr getBookLevelIter(side_t side) const;
    virtual size_t getNumLevels(side_t side) const;

    virtual void onEvent(const Msg& msg);

private:
    typedef std::vector<BookLevelPtr>   BookLevelVec;
    typedef std::bitset<32> SeenLevelsSet;
    typedef std::set<long> PriceSet;

    void onEvent(const ProtoBufMsgBase& msg);
    void onBadState();

    virtual void onSMonStatusChange(source_t src, smon_status_t status, smon_status_t old_status,
            const ptime_duration_t& avg_lag_tv, const ptime_duration_t& last_interval_tv);

    template<side_t side>
    std::pair<size_t,bool> tryAddLevel(const double price, const int32_t size);

    template<side_t side>
    size_t tryDropLevelsByIndex(SeenLevelsSet& seen_levels);

    template<side_t side>
    size_t tryDropLevelsByPrice(PriceSet& marked_prices);

    template <side_t side>
    int32_t updateBookLevelsFullSnapshot(const ::google::protobuf::RepeatedPtrField< acr::SzseMarketSnap::DepthLevel >& szse_book_side);

    long toLong(double price) { return (long)(price * 1000); }  // transform price to avoid comparing doubles

    ClientContextPtr    m_spCC;
    SourceMonitorPtr    m_spSMon;
    uint32_t            m_verbose;

    mutable bool        m_ok;
    mutable bool        m_ok_dirty;

    bool                m_full_snapshot_received;
    BookLevelVec        m_bookLevels[2];    // BID = 0, ASK = 1

    EventSubPtr         m_eventSub;

    const uint32_t m_kMktDataCompleteBodyTypeMask;
    typedef boost::unordered_map<uint32_t, acr::SzseMarketSnap> SplitSeq2MktDataCache;
    SplitSeq2MktDataCache m_splitSeq2MktDataMap;

};
BB_DECLARE_SHARED_PTR(SzseBook);

/// BookSpec corresponding to an Wind book
class SzseBookSpec : public SourceBookSpec
{
public:
    BB_DECLARE_SCRIPTING();

    SzseBookSpec() : SourceBookSpec() {}
    SzseBookSpec(const bb::SzseBookSpec&, const boost::optional<bb::InstrSubst>&);
    SzseBookSpec(const instrument_t &instr, source_t src)
        : SourceBookSpec(instr, src)
    {}

    virtual IBookPtr build(BookBuilder *builder) const;
    virtual void print(std::ostream &o, const LuaPrintSettings &ps) const;
    virtual SzseBookSpec *clone(const boost::optional<InstrSubst> &instrSubst = boost::none) const;
};
BB_DECLARE_SHARED_PTR(SzseBookSpec);

}

#endif // BB_CLIENTCORE_SZSEBOOK_H
