#include <bb/clientcore/EventDist.h>
#include <bb/clientcore/ClientContext.h>
#include <bb/clientcore/MsgHandler.h>
#include <boost/bind.hpp>

#include "SzseTickProvider.h"

// Contents Copyright 2015 Athena Capital Research LLC. All Rights Reserved.

namespace bb {

SzseTickProvider::SzseTickProvider(const ClientContextPtr &ctx, const instrument_t &instr, source_t source, const std::string &desc)
    : TickProviderImpl(source, instr, desc, true),
      m_bTickReceived(false),
      m_notionalTurnover(0),
      m_subTradeMsg(MsgHandler::create<ProtoBufMsgBase>(source, m_instr, ctx->getEventDistributor(),
                  boost::bind(&SzseTickProvider::onTradeMsg, this, _1), PRIORITY_CC_Ticks))
{
}

void SzseTickProvider::onTradeMsg(const ProtoBufMsgBase &msg)
{
    //stock only here
    if(msg.hdr->source.type() != getSource().type())
        return;

    const acr::SzseTickTbt* tickTbt = dynamic_cast<const acr::SzseTickTbt*>(&(*msg.getMsg()));
    if(tickTbt != NULL)
    {
        if(unlikely(tickTbt->symid() != m_instr.sym.id()))
            return;

        m_bTickReceived = true;

        const acr::SzseTickTbt& tick = *tickTbt;
        incrementTotalVolume(tick.last_qty());

        timeval_t tradeTime(tick.transact_time());
        std::cout << "onTradeMsg transact_time=" << tradeTime.toString() << std::endl;
        TradeTick tt(tick.last_price(),
                     tick.last_qty(),
                     msg.hdr->source,
                     tradeTime,
                     msg.hdr->time_sent,
                     msg.hdr->seq,
                     false,
                     m_instr.mkt,
                     SIDE_BOTH);

        m_notionalTurnover = (tick.last_price() * tick.last_qty());

        setLastTimestamps(tradeTime, msg.hdr->time_sent);
        setLastTradeTick(tt);
        notifyTickReceived();
    }
}

}
