#include <iostream>
#include <map>

#include <boost/static_assert.hpp>
#include <boost/foreach.hpp>

#include <bb/core/bbassert.h>
#include <bb/core/messages.h>
#include <bb/core/Log.h>
#include <bb/core/LuabindScripting.h>
#include <bb/core/LuaCodeGen.h>

#include <bb/clientcore/BookBuilder.h>
#include <bb/clientcore/ClientContext.h>
#include <bb/core/MessageMacros.h>
#include <bb/clientcore/SourceMonitor.h>

#include "OrderDrivenSzseBook.h"

namespace bb {

namespace detail {

class OrderDrivenSzseBookLevelCIter
    : public IBookLevelCIter
{
public:
    virtual ~OrderDrivenSzseBookLevelCIter()
    {}

    virtual bool hasNext() const
    {
        for (std::list<BookLevelPtr>::const_iterator iter = m_curr_iter; iter != m_end_iter; ++iter)
            if (*iter)
            {
                return true;
            }
        return false;
    }

    virtual BookLevelCPtr next()
    {
        while (m_curr_iter != m_end_iter)
        {
            if (BookLevelCPtr spBL = *(m_curr_iter++))
            {
                return spBL;
            }
        }
        return BookLevelCPtr();
    }

protected:
    friend class bb::OrderDrivenSzseBook;

    OrderDrivenSzseBookLevelCIter(const std::list<BookLevelPtr>& v)
        : m_curr_iter(v.begin())
        , m_end_iter(v.end())
    {}

private:
    std::list<BookLevelPtr>::const_iterator m_curr_iter;
    std::list<BookLevelPtr>::const_iterator m_end_iter;
};

} // namespace detail

OrderDrivenSzseBook::OrderDrivenSzseBook(const instrument_t& instr, ClientContextPtr spCC, source_t src,
                  const std::string& strDesc, SourceMonitorPtr spSMon)
    : BookImpl(instr, strDesc, src, true) // true => isAggregate
    , m_spCC(spCC)
    , m_spSMon(spSMon)
    , m_ok(false)
    , m_ok_dirty(true)
{
    if (!m_spCC)
    {
        throw std::invalid_argument("ClientContextPtr spCC");
    }
    if (!m_spCC->getEventDistributor())
    {
        throw std::invalid_argument("EventDistributorPtr: spCC->getED");
    }

    m_verbose = m_spCC->getVerbose();

    m_spCC->getEventDistributor()->subscribeEvents(m_eventSub, this, src, MSG_PROTOBUF, m_instr, PRIORITY_CC_Book);

    if (m_spSMon)
    {
        m_spSMon->addSMonListener(this);
    }

    if (m_verbose)
    {
        LOG_INFO << "Constructed OrderDrivenSzseBook for " << m_instr << bb::endl;
    }
}

OrderDrivenSzseBook::~OrderDrivenSzseBook()
{
    // disconnect from SourceMonitor
    if (m_spSMon)
    {
        m_spSMon->removeSMonListener(this);
    }
}

void OrderDrivenSzseBook::flushBook()
{
    m_ok_dirty = true;
    if (m_verbose >= 2)
    {
        LOG_INFO << "OrderDrivenSzseBook: " << m_desc << " flushBook" << bb::endl;
    }

    m_bookLevels[BID].clear();
    m_bookLevels[ASK].clear();

    notifyBookFlushed(NULL);
}

PriceSize OrderDrivenSzseBook::getNthSide(size_t depth, side_t side) const
{
    if (m_bookLevels[side].size() > depth)
    {
        const BookLevelList& sideLevels = m_bookLevels[side];
        BookLevelList::const_iterator iter = sideLevels.begin();
        size_t index = 0;
        while(iter != sideLevels.end()) {
            if (index == depth) {
                return (*iter)->getPriceSize();
            }

            index++;
            iter++;
        }
    }

    return PriceSize();
}

IBookLevelCIterPtr OrderDrivenSzseBook::getBookLevelIter(side_t side) const
{
    return IBookLevelCIterPtr(new detail::OrderDrivenSzseBookLevelCIter(m_bookLevels[side]));
}

/// Returns the number of levels of a particular side of this book.
size_t OrderDrivenSzseBook::getNumLevels(side_t side) const
{
    const BookLevelList& sideLevels = m_bookLevels[side];
    int32_t num_levels = sideLevels.size();

    for (BookLevelList::const_reverse_iterator iter = sideLevels.rbegin(); iter != sideLevels.rend(); ++iter)
    {
        if (*iter)
        {
            break;
        }
        --num_levels;
    }
    return num_levels;
}

/// EventListener interface
void OrderDrivenSzseBook::onEvent(const Msg& msg)
{
    BB_MESSAGE_SWITCH_BEGIN(&msg)
        BB_MESSAGE_CASE(ProtoBufMsgBase, pMsgDepth)
            onEvent(*pMsgDepth);
        BB_MESSAGE_CASE_END()
    BB_MESSAGE_SWITCH_END()
}

double OrderDrivenSzseBook::getMidPrice() const {
    const BookLevelList& bid = m_bookLevels[BID];
    const BookLevelList& ask = m_bookLevels[ASK];

    bool isBidEmpty = (0 == bid.size()) ? true : false;
    bool isAskEmpty = (0 == ask.size()) ? true : false;
    if (isBidEmpty && isAskEmpty) {
        return 0;
    }

    double bestBidPrice = 0;
    double bestAskPrice = 0;

    if (!isBidEmpty) {
        BookLevelPtr bl = *(bid.begin());
        bestBidPrice = bl->getPrice();
    }

    if (!isAskEmpty) {
        BookLevelPtr bl = *(ask.begin());
        bestAskPrice = bl->getPrice();
    }

    if (isBidEmpty) {
        return bestAskPrice;
    }

    if (isAskEmpty) {
        return bestBidPrice;
    }

    return ((bestBidPrice + bestAskPrice) / 2.0);
}

void OrderDrivenSzseBook::onMarketTbt(const ProtoBufMsgBase& msg, acr::SzseMarketTbt* mkt) {
    if(!(mkt->has_symid())) {
        return;
    }

    if (unlikely(mkt->symid() != m_instr.sym.id()))
    {
        return;
    }

    if(!(mkt->has_side())) {
        return;
    }

    if(!(mkt->has_price())) {
        return;
    }

    if(!(mkt->has_order_qty())) {
        return;
    }

    if(!(mkt->has_appl_seq_num())) {
        return;
    }

    std::string sd = mkt->side();
    side_t side;
    if (std::string("1") == sd) { // TODO strncmp fix
        side = BID;
    } else if (std::string("2") == sd) {
        side = ASK;
    } else {
        return;
    }

    double price = mkt->price();
    int32_t size = mkt->order_qty();
    uint64_t id = mkt->appl_seq_num();

    if (BID == side) {
        tryAddOrder2Level<BID>(msg, price, size, id);
    } else if (ASK == side) {
        tryAddOrder2Level<ASK>(msg, price, size, id);
    }
}

void OrderDrivenSzseBook::updateSideLevel(BookLevelList& sideLevels, OrderInfoPtr info, uint64_t id, int64_t size, bool isCancelled) {
    if (!info) {
        return;
    }

    BookLevelPtr bl = info->bl;
    BookOrder* order = info->order;
    BookLevelList::iterator iter = sideLevels.begin();
    int32_t levelSize = bl->getSize();
    int32_t& orderSize = order->sz;
    while(iter != sideLevels.end())
    {
        if (&*(bl) != &*(*iter))
        {
            iter++;
            continue;
        }

        // update order
        if ((orderSize <= size) || isCancelled) {
            bl->removeAndDeleteOrder(order);

            if (levelSize <= size) {
                sideLevels.erase(iter);

                notifyBookLevelDropped(bl.get());
            }

            m_seqNums.erase(id);
        } else {
            orderSize -= size;
            levelSize -= size;
            bl->setSize(levelSize);

            notifyBookLevelModified(bl.get());
        }

        break;
    }
}

void OrderDrivenSzseBook::onTickTbt(const ProtoBufMsgBase& msg, acr::SzseTickTbt* tick) {
    if(!(tick->has_symid())) {
        return;
    }

    if (unlikely(tick->symid() != m_instr.sym.id()))
    {
        return;
    }

    if(!(tick->has_last_price())) {
        return;
    }

    if(!(tick->has_last_qty())) {
        return;
    }

    if(!(tick->has_bid_appl_seq_num())) {
        return;
    }

    if(!(tick->has_offer_appl_seq_num())) {
        return;
    }

    if(!(tick->has_exec_type())) {
        return;
    }

    bool isCancelled;
    std::string et = tick->exec_type();
    if (std::string("4") == et) { // Cancelled
        isCancelled = true;
    } else if (std::string("F") == et) { // Trade 
        isCancelled = false;
    } else {
        return;
    }

    int64_t size = tick->last_qty();

    // delete from the 'BID' bookLevel
    OrderInfoPtr bidOrderInfo;
    bidOrderInfo.reset();
    OrderInfoPtr askOrderInfo;
    askOrderInfo.reset();
    uint64_t bidNum = tick->bid_appl_seq_num();
    uint64_t askNum = tick->offer_appl_seq_num();
    if (0 != bidNum) {
        SeqNumMap::iterator it = m_seqNums.find(bidNum);
        if (m_seqNums.end() != it) {
            bidOrderInfo = it->second;
        }
    }

    if (0 != askNum) {
        SeqNumMap::iterator it = m_seqNums.find(askNum);
        if (m_seqNums.end() != it) {
            askOrderInfo = it->second;
        }
    }

    updateSideLevel(m_bookLevels[BID], bidOrderInfo, bidNum, size, isCancelled);
    updateSideLevel(m_bookLevels[ASK], askOrderInfo, askNum, size, isCancelled);
}

void OrderDrivenSzseBook::onEvent(const ProtoBufMsgBase& msg)
{
    if (msg.hdr->source.type() != getSource().type())
    {
        return;
    }

    acr::SzseMarketTbt* mkt = dynamic_cast<acr::SzseMarketTbt* >(&(*msg.getMsg()));
    if (mkt != NULL)
    {
        onMarketTbt(msg, mkt);
        return;
    }

    acr::SzseTickTbt* tick = dynamic_cast<acr::SzseTickTbt* >(&(*msg.getMsg()));
    if (tick != NULL)
    {
        onTickTbt(msg, tick);
        return;
    }
}

template<side_t side>
void OrderDrivenSzseBook::tryAddOrder2Level(const ProtoBufMsgBase& msg, const double price, const int32_t orderSize, const uint64_t orderID)
{
    BookLevelPtr targetLevel;
    targetLevel.reset();
    size_t levelIndex = 0;

    BookLevelList& sideLevels (m_bookLevels[side]);
    BookLevelList::iterator iter = sideLevels.begin();
    while(iter != sideLevels.end())
    {
        BookLevelPtr bl = *iter;
        if (bb::EQ(bl->getPrice(), price))
        {
            targetLevel = bl;

            break;
        }

        iter++;
        levelIndex++;
    }

    // try to create new level
    if (NULL == targetLevel) {
        BookLevelPtr level(boost::make_shared<BookLevel>(m_src, side, price, orderSize));
        // level->setMaintainOrderList(false);
        // level->setUseOrderListSizes(false);
        level->clearAndDeleteOrders();
        BookLevelList::iterator spot = std::lower_bound(sideLevels.begin(),
                sideLevels.end(),
                level,
                BookLevelComparer<side>());
        spot = sideLevels.insert(spot, level);
        levelIndex = std::distance(sideLevels.begin(),spot);

        notifyBookLevelAdded (level.get());

        targetLevel = level;
    }

    // append the incoming new order
    BookOrder* order = new BookOrder(side, price, orderSize, targetLevel->getSource());
    order->idnum = orderID;
    targetLevel->pushbackOrder(order);

    // also cache locally
    OrderInfoPtr oi(new OrderInfo(targetLevel, order));
    m_seqNums.insert(std::make_pair(order->idnum, oi));

    // the 'bookchanged' notification should be skipped while crossing.
    // side can only be 'BID' or 'ASK' here
    side_t opsSide = ((BID == side) ? ASK : BID);
    if (m_bookLevels[opsSide].begin() != m_bookLevels[opsSide].end()) {
        BookLevelPtr bl = *(m_bookLevels[opsSide].begin());
        if ((bl->getSize()) > 0) {
            if (BID == opsSide) {
                if (bb::GE(bl->getPrice(), price)) {
                    return;
                }
            } else {
                if (bb::GE(price, bl->getPrice())) {
                    return;
                }
            }
        }
    }

    m_lastChangeTv = msg.hdr->time_sent;

    // notify book changed
    if (BID == side) {
        notifyBookChanged(&msg, levelIndex, -1);
    } else {
        notifyBookChanged(&msg, -1, levelIndex);
    }
}

void OrderDrivenSzseBook::onBadState()
{
    m_ok_dirty = false;
    book_rdy_fg = false;
}

void OrderDrivenSzseBook::onSMonStatusChange(
    source_t src, smon_status_t status, smon_status_t old_status,
    const ptime_duration_t& avg_lag_tv, const ptime_duration_t& last_interval_tv)
{
    m_ok_dirty = true;
    if (status == Stat_SMon_OK)
    {
        m_source_ok = true;
        if (m_verbose >= 1)
        {
            LOG_INFO << m_desc << ": source is OK again, book will be marked ready" << bb::endl;
        }
    }
    else // !Stat_SMon_OK
    {
        m_source_ok = false;
        book_rdy_fg = false;
        if (status == Stat_SMon_SourceDown)
        {
            if (m_verbose >= 1)
            {
                LOG_INFO << m_desc << ": Source is down, flushing book and marking as invalid" << bb::endl;
            }
            flushBook();
        }
        else
        {
            if (m_verbose >= 1)
            {
                LOG_INFO << m_desc << ": Source is having problems, book will be marked invalid until source is ok" << bb::endl;
            }
        }
    }
}

OrderDrivenSzseBookSpec::OrderDrivenSzseBookSpec(const bb::OrderDrivenSzseBookSpec &a, const boost::optional<bb::InstrSubst> &instrSubst)
    : SourceBookSpec(a, instrSubst)
{
}

bool OrderDrivenSzseBookSpec::registerScripting(lua_State &state)
{
    luabind::module(&state)
    [
        luabind::class_<OrderDrivenSzseBookSpec, SourceBookSpec, IBookSpecPtr>("OrderDrivenSzseBookSpec")
            .def(luabind::constructor<>())
    ];
    return true;
}

IBookPtr OrderDrivenSzseBookSpec::build(BookBuilder *builder) const
{
    if (m_source.type() == SRC_SZSE_DIRECT)
    {
        const std::string symname(m_instrument.sym.toString());
        ClientContextPtr spContext = builder->getClientContext();
        RandomSourcePtr spRandomSource = builder->getRandomSource();

        SourceMonitorPtr spSMon = builder->getUseSrcMonitors() ? spContext->getSourceMonitor(m_source, true) : SourceMonitorPtr();
        std::auto_ptr<OrderDrivenSzseBook> book(new OrderDrivenSzseBook(m_instrument, spContext, m_source, "OrderDrivenSzseBook_"+symname, spSMon));
        return IBookPtr(book);
    }
    BB_THROW_ERROR_SS("OrderDrivenSzseBookSpec::build: Unhandled source " << m_source);
}

void OrderDrivenSzseBookSpec::print(std::ostream& os, const LuaPrintSettings &ps) const
{
    const LuaPrintSettings onei = ps.next(); // indentation one past current

    os << "(function () -- OrderDrivenSzseBookSpec" << std::endl
      << onei.indent() << "local book = OrderDrivenSzseBookSpec()" << std::endl
      << onei.indent() << "book.instrument = " << luaMode(m_instrument, onei) << std::endl
      << onei.indent() << "book.source = " << luaMode(m_source, onei) << std::endl
      << onei.indent() << "return book" << std::endl
      << onei.indent() << "end)()";
}

OrderDrivenSzseBookSpec* OrderDrivenSzseBookSpec::clone(const boost::optional<InstrSubst> &instrSubst) const
{
    return new OrderDrivenSzseBookSpec(*this, instrSubst);
}

}
