#include <iostream>
#include <map>

#include <boost/static_assert.hpp>
#include <boost/foreach.hpp>

#include <bb/core/bbassert.h>
#include <bb/core/messages.h>
#include <bb/core/Log.h>
#include <bb/core/LuabindScripting.h>
#include <bb/core/LuaCodeGen.h>

#include <bb/clientcore/BookBuilder.h>
#include <bb/clientcore/ClientContext.h>
#include <bb/core/MessageMacros.h>
#include <bb/clientcore/SourceMonitor.h>

#include "SzseBook.h"

namespace bb {

namespace detail {

class SzseBookLevelCIter
    : public IBookLevelCIter
{
public:
    virtual ~SzseBookLevelCIter()
    {}

    virtual bool hasNext() const
    {
        for (std::vector<BookLevelPtr>::const_iterator iter = m_curr_iter; iter != m_end_iter; ++iter)
            if (*iter)
            {
                return true;
            }
        return false;
    }

    virtual BookLevelCPtr next()
    {
        while (m_curr_iter != m_end_iter)
        {
            if (BookLevelCPtr spBL = *(m_curr_iter++))
            {
                return spBL;
            }
        }
        return BookLevelCPtr();
    }

protected:
    friend class bb::SzseBook;

    SzseBookLevelCIter(const std::vector<BookLevelPtr>& v)
        : m_curr_iter(v.begin())
        , m_end_iter(v.end())
    {}

private:
    std::vector<BookLevelPtr>::const_iterator m_curr_iter;
    std::vector<BookLevelPtr>::const_iterator m_end_iter;
};

} // namespace detail

SzseBook::SzseBook(const instrument_t& instr, ClientContextPtr spCC, source_t src,
                  const std::string& strDesc, SourceMonitorPtr spSMon)
    : BookImpl(instr, strDesc, src, true) // true => isAggregate
    , m_spCC(spCC)
    , m_spSMon(spSMon)
    , m_ok(false)
    , m_ok_dirty(true)
    , m_full_snapshot_received(false)
    , m_kMktDataCompleteBodyTypeMask(acr::SzseMarketSnap::COMMON | acr::SzseMarketSnap::BIDLEVEL | acr::SzseMarketSnap::OFFERLEVEL)
{
    if (!m_spCC)
    {
        throw std::invalid_argument("ClientContextPtr spCC");
    }
    if (!m_spCC->getEventDistributor())
    {
        throw std::invalid_argument("EventDistributorPtr: spCC->getED");
    }

    m_verbose = m_spCC->getVerbose();

    m_spCC->getEventDistributor()->subscribeEvents(m_eventSub, this, src, MSG_PROTOBUF, m_instr, PRIORITY_CC_Book);

    if (m_spSMon)
    {
        m_spSMon->addSMonListener(this);
    }

    if (m_verbose)
    {
        LOG_INFO << "Constructed SzseBook for " << m_instr << bb::endl;
    }
}

SzseBook::~SzseBook()
{
    // disconnect from SourceMonitor
    if (m_spSMon)
    {
        m_spSMon->removeSMonListener(this);
    }
}

void SzseBook::flushBook()
{
    m_ok_dirty = true;
    if (m_verbose >= 2)
    {
        LOG_INFO << "SzseBook: " << m_desc << " flushBook" << bb::endl;
    }

    m_bookLevels[BID].clear();
    m_bookLevels[ASK].clear();

    notifyBookFlushed(NULL);
}

PriceSize SzseBook::getNthSide(size_t depth, side_t side) const
{
    if (m_bookLevels[side].size() > depth)
    {
        if (const BookLevelPtr& spBookLevel = m_bookLevels[side][depth])
        {
            return spBookLevel->getPriceSize();
        }
    }

    return PriceSize();
}


IBookLevelCIterPtr SzseBook::getBookLevelIter(side_t side) const
{
    return IBookLevelCIterPtr(new detail::SzseBookLevelCIter(m_bookLevels[side]));
}


/// Returns the number of levels of a particular side of this book.
size_t SzseBook::getNumLevels(side_t side) const
{
    const BookLevelVec& sideLevels = m_bookLevels[side];
    int32_t num_levels = sideLevels.size();

    for (BookLevelVec::const_reverse_iterator iter = sideLevels.rbegin(); iter != sideLevels.rend(); ++iter)
    {
        if (*iter)
        {
            break;
        }
        --num_levels;
    }
    return num_levels;
}

/// EventListener interface
void SzseBook::onEvent(const Msg& msg)
{
    BB_MESSAGE_SWITCH_BEGIN(&msg)
        BB_MESSAGE_CASE(ProtoBufMsgBase, pMsgDepth)
            onEvent(*pMsgDepth);
        BB_MESSAGE_CASE_END()
    BB_MESSAGE_SWITCH_END()
}

void SzseBook::onEvent(const ProtoBufMsgBase& msg)
{
    if (msg.hdr->source.type() != getSource().type())
    {
        return;
    }

    acr::SzseMarketSnap* mktDataCur = dynamic_cast<acr::SzseMarketSnap* >(&(*msg.getMsg()));
    if (unlikely(mktDataCur == NULL))
    {
        return;
    }

    if (unlikely(mktDataCur->symid() != m_instr.sym.id()))
    {
        return;
    }

    uint32_t splitSeq = 0;
    if (likely(mktDataCur->has_split_info()))
    {
        splitSeq = mktDataCur->split_info().seq();
        SplitSeq2MktDataCache::iterator iter = m_splitSeq2MktDataMap.find(splitSeq);

        if (m_splitSeq2MktDataMap.end() == iter)  //first see
        {
            m_splitSeq2MktDataMap.insert(std::make_pair(splitSeq, *mktDataCur));
            return;
        }
        else
        {
            acr::SzseMarketSnap& cachedSnap = iter->second;

            const uint32_t mask = cachedSnap.split_info().bodytype_mask();
            const uint32_t incomingMask = mktDataCur->split_info().bodytype_mask();

            if (acr::SzseMarketSnap::BIDLEVEL == incomingMask)
            {
                cachedSnap.mutable_bid()->Swap(mktDataCur->mutable_bid());
            }
            else if (acr::SzseMarketSnap::OFFERLEVEL == incomingMask)
            {
                cachedSnap.mutable_ask()->Swap(mktDataCur->mutable_ask());
            }
            else
            {
                cachedSnap.MergeFrom(*mktDataCur);
            }

            cachedSnap.mutable_split_info()->set_bodytype_mask(mask | incomingMask);
            if (cachedSnap.split_info().bodytype_mask() != m_kMktDataCompleteBodyTypeMask)
            {
                return;
            }
            else
            {
                mktDataCur = &cachedSnap;
            }
        }
    }

    m_ok_dirty = true;
    book_rdy_fg = true;
    m_full_snapshot_received = true;

    if (mktDataCur->has_total_value_trade())
    {
        m_turnover = mktDataCur->total_value_trade();
    }

    if (mktDataCur->has_total_volume_trade())
    {
        m_totalVolume = mktDataCur->total_volume_trade();
    }

    if (mktDataCur->has_latest_price()) 
    {
        m_lastTradePrice = mktDataCur->latest_price();
    }

    int32_t bid_level_changed = updateBookLevelsFullSnapshot<BID>(mktDataCur->bid());
    int32_t ask_level_changed = updateBookLevelsFullSnapshot<ASK>(mktDataCur->ask());
    if (bid_level_changed != -1 || ask_level_changed != -1)
    {
        m_lastChangeTv = msg.hdr->time_sent;
        notifyBookChanged(&msg, bid_level_changed, ask_level_changed);
    }

    if (splitSeq != 0)
    {
        m_splitSeq2MktDataMap.erase(splitSeq);
    }
}

template<side_t side>
std::pair<size_t, bool> SzseBook::tryAddLevel(const double price, const int32_t size)
{
    BookLevelVec& sideLevels (m_bookLevels[side]);
    for(size_t j = 0; j < sideLevels.size(); j++)
    {
        if (bb::EQ(sideLevels[j]->getPrice(), price))
        {
            if (size != sideLevels[j]->getSize())
            {
                sideLevels[j]->setPriceSize(price, size);
                notifyBookLevelModified(sideLevels[j].get());
                return std::make_pair(j, true);
            }
            else
            {
                return std::make_pair(j, false);
            }
        }
    }

    BookLevelPtr level(boost::make_shared<BookLevel>(m_src, side, price, size));
    level->setMaintainOrderList(false);
    level->setUseOrderListSizes(false);
    BookLevelVec::iterator spot = std::lower_bound(sideLevels.begin(),
            sideLevels.end(),
            level,
            BookLevelComparer<side>());
    spot = sideLevels.insert(spot, level);
    notifyBookLevelAdded (level.get());
    const std::size_t added_index (std::distance(sideLevels.begin(),spot));
    return std::make_pair(added_index,true);
}

template<side_t side>
size_t SzseBook::tryDropLevelsByIndex (SeenLevelsSet& seen_levels)
{
    size_t first_level_changed (std::numeric_limits<size_t>::max());
    BookLevelVec & existing_levels (m_bookLevels[side]);
    for(size_t i = 0 ; i < existing_levels.size(); i++)
    {
        if (!seen_levels[i])
        {
            notifyBookLevelDropped(existing_levels[ i ].get());
            first_level_changed = std::min (first_level_changed, i);
        }
    }

    BookLevelVec::iterator iter = existing_levels.begin();
    size_t i=0;
    while(iter != existing_levels.end())
    {
        if (!seen_levels[i++])
        {
            iter = existing_levels.erase(iter);
        }
        else
            iter++;
    }
    return first_level_changed;
}

template<side_t side>
size_t SzseBook::tryDropLevelsByPrice(PriceSet& marked_prices)
{
    size_t first_level_changed (std::numeric_limits<size_t>::max());
    BookLevelVec & existing_levels (m_bookLevels[side]);
    for(size_t i = 0 ; i < existing_levels.size(); i++)
    {
        if (marked_prices.find(toLong(existing_levels[i]->getPrice())) != marked_prices.end())
        {
            notifyBookLevelDropped(existing_levels[i].get());
            first_level_changed = std::min (first_level_changed, i);
        }
    }

    BookLevelVec::iterator iter = existing_levels.begin();
    while(iter != existing_levels.end())
    {
        if (marked_prices.find(toLong((*iter)->getPrice())) != marked_prices.end())
        {
            iter = existing_levels.erase(iter);
        }
        else
            iter++;
    }
    return first_level_changed;
}

template<side_t side>
int32_t SzseBook::updateBookLevelsFullSnapshot(const ::google::protobuf::RepeatedPtrField<acr::SzseMarketSnap::DepthLevel>& depthLevels)
{
    SeenLevelsSet seen_levels;
    size_t first_level_changed(std::numeric_limits<size_t>::max());

    for(int i = 0; i < depthLevels.size(); ++i)
    {
        const acr::SzseMarketSnap::DepthLevel& nl = depthLevels.Get(i);
        if (nl.size() <= 0 || nl.price() <= 0)
        {
            continue;
        }

        const std::pair<size_t, bool> added_or_modified_level = tryAddLevel<side>(nl.price(), nl.size());
        seen_levels.set (added_or_modified_level.first);
        if (added_or_modified_level.second) // this level did change
        {
            first_level_changed = std::min (first_level_changed, added_or_modified_level.first);
        }

        BookLevel& level = *m_bookLevels[side][added_or_modified_level.first];
        level.setNumOrders(nl.number_of_orders());
        level.clearAndDeleteOrders();
        for(int j=0; j < nl.orders_size(); ++j)
        {
            BookOrder* bo = new BookOrder(side, nl.price(), nl.orders(j), level.getSource());
            level.pushbackOrder(bo);
        }
    }

    const size_t first_deleted_level(tryDropLevelsByIndex<side>(seen_levels));
    first_level_changed = std::min(first_level_changed, first_deleted_level);

    return first_level_changed != std::numeric_limits<size_t>::max() ?
        static_cast<int32_t>(first_level_changed) : -1;
}

void SzseBook::onBadState()
{
    m_ok_dirty = false;
    m_full_snapshot_received = false;
    book_rdy_fg = false;
}

void SzseBook::onSMonStatusChange(
    source_t src, smon_status_t status, smon_status_t old_status,
    const ptime_duration_t& avg_lag_tv, const ptime_duration_t& last_interval_tv)
{
    m_ok_dirty = true;
    if (status == Stat_SMon_OK)
    {
        m_source_ok = true;
        if (m_verbose >= 1)
        {
            LOG_INFO << m_desc << ": source is OK again, book will be marked ready" << bb::endl;
        }
    }
    else // !Stat_SMon_OK
    {
        m_source_ok = false;
        m_full_snapshot_received = false;
        book_rdy_fg = false;
        if (status == Stat_SMon_SourceDown)
        {
            if (m_verbose >= 1)
            {
                LOG_INFO << m_desc << ": Source is down, flushing book and marking as invalid" << bb::endl;
            }
            flushBook();
        }
        else
        {
            if (m_verbose >= 1)
            {
                LOG_INFO << m_desc << ": Source is having problems, book will be marked invalid until source is ok" << bb::endl;
            }
        }
    }
}

SzseBookSpec::SzseBookSpec(const bb::SzseBookSpec &a, const boost::optional<bb::InstrSubst> &instrSubst)
    : SourceBookSpec(a, instrSubst)
{
}

bool SzseBookSpec::registerScripting(lua_State &state)
{
    luabind::module(&state)
    [
        luabind::class_<SzseBookSpec, SourceBookSpec, IBookSpecPtr>("SzseBookSpec")
            .def(luabind::constructor<>())
    ];
    return true;
}

IBookPtr SzseBookSpec::build(BookBuilder *builder) const
{
    if (m_source.type() == SRC_SZSE_DIRECT)
    {
        const std::string symname(m_instrument.sym.toString());
        ClientContextPtr spContext = builder->getClientContext();
        RandomSourcePtr spRandomSource = builder->getRandomSource();

        SourceMonitorPtr spSMon = builder->getUseSrcMonitors() ? spContext->getSourceMonitor(m_source, true) : SourceMonitorPtr();
        std::auto_ptr<SzseBook> book(new SzseBook(m_instrument, spContext, m_source, "SzseBook_"+symname, spSMon));
        return IBookPtr(book);
    }
    BB_THROW_ERROR_SS("SzseBookSpec::build: Unhandled source " << m_source);
}

void SzseBookSpec::print(std::ostream& os, const LuaPrintSettings &ps) const
{
    const LuaPrintSettings onei = ps.next(); // indentation one past current

    os << "(function () -- SzseBookSpec" << std::endl
      << onei.indent() << "local book = SzseBookSpec()" << std::endl
      << onei.indent() << "book.instrument = " << luaMode(m_instrument, onei) << std::endl
      << onei.indent() << "book.source = " << luaMode(m_source, onei) << std::endl
      << onei.indent() << "return book" << std::endl
      << onei.indent() << "end)()";
}

SzseBookSpec* SzseBookSpec::clone(const boost::optional<InstrSubst> &instrSubst) const
{
    return new SzseBookSpec(*this, instrSubst);
}

}
